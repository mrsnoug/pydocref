import json
import mmap
import os
import re
from functools import reduce
from hashlib import sha256

from tqdm import tqdm


class Scanner:
    """
    Class representing the Scanner. It is responsible for scanning files and detecting 
    changes. If change is detected or it is a new file then it needs to read it and fetch
    variables from it. The way it knows it is because of the JSON file representing each file
    of interest (it contains the hash of a file and its variables).

    Attributes
    ----------
    BLOCK_SIZE : int
        defines the chunk size. Each read from the file will be of this size

    SOFTWARE_DIR : str
        string containg absolute path to the directory with .py files

    ROOT_DIR : str
        absolute path to the root directory, all relevant files should be contained here

    JSON_FILES_DIR : str
        absolute path to the directory with JSON files

    Methods
    ----------
    calculateHash(file):
        calculates the SHA256 hash of a given file and returns it.

    __init__():
        constructor, it checks if JSON_FILES_DIR exists and if not then creates one. Then it
        creates a dictionary with all files from ROOT_DIR.
    
    scan():
        wrapper method that runs other methods. Sort of a main().

    get_directory_structure(rootdir):
        maps all the files and directories into a nested dictionary.

    delete_software_elements(dict):
        takes dict and deletes all keys with name starting with ".".

    sanitize_directory_structure(dict, depth):
        for a given dict it calculates the hashes of a files (keys that do not correspond to
        other dictionaries).

    create_json_files():
        based on a file_dict it creates the JSON files if they do not exist. It only creates 
        them and inserts their sha256 hash into it and set's "scan_file" field to true. 
        If there is a file with that specific name and the hashes are equal it does nothing.
        If there is a file with that specific name, but the hashes are different then it 
        inserts new hash and a field scan_file set to true. scan_file indicates if a text 
        file associated with that specific JSON needs to be parsed.

    parse_files():
        lists the ".dependency" directory for JSON files. For each JSON file it checks
        if parsing is needed. If so then it runs other methods (listed below) that will
        fetch the variables in corresponding text files and update the JSON in file.

    parse_single_file(dep_name):
        Given a JSON file name it opens corresponding text file and parses it. It uses
        regular expressions to find variable assignment pattern. It constructs a dictionary
        with said variables and returns it.

    count_lines(path):
        it counts the lines of a file with a given path. It is used for tqdm to look nicely.
    """

    BLOCK_SIZE = 65536
    SOFTWARE_DIR = os.getcwd()
    ROOT_DIR = os.path.abspath(os.path.join(SOFTWARE_DIR, ".."))
    JSON_FILES_DIR = os.path.join(ROOT_DIR, ".dependency")

    def __init__(self):
        if not os.path.exists(self.JSON_FILES_DIR):
            # create directory for json files. This should be run only on the first try
            os.mkdir(self.JSON_FILES_DIR)

        self.file_dict = self.get_directory_structure(self.ROOT_DIR)

    def scan(self):
        self.file_dict = self.delete_software_elements(self.file_dict)
        temp_root = os.path.abspath((os.path.join(self.ROOT_DIR, "..")))
        self.sanitize_directory_structure(self.file_dict, [temp_root])
        self.create_json_files(self.file_dict, [])
        self.parse_files()

    def calculate_hash(self, file_path: str):
        """ Method calculating SHA256 value for a given file """
        file_hash = sha256()
        with open(file_path, "rb") as file:
            fb = file.read(self.BLOCK_SIZE)
            while len(fb) > 0:
                file_hash.update(fb)
                fb = file.read(self.BLOCK_SIZE)

        return file_hash.hexdigest()

    def get_directory_structure(self, rootdir):
        """Given the root directory it constructs the dictionary with directories and files."""

        structure = {}
        rootdir = rootdir.rstrip(os.sep)
        start = rootdir.rfind(os.sep) + 1
        for path, dirs, files in os.walk(rootdir):
            folders = path[start:].split(os.sep)
            subdir = dict.fromkeys(files)
            parent = reduce(dict.get, folders[:-1], structure)
            parent[folders[-1]] = subdir

        return structure

    def delete_software_elements(self, structure: dict):
        """Method that deletes all entries (keys) which names start with a dot."""

        filtered_dict = {k: v for (k, v) in structure.items() if not k.startswith(".")}
        for key in filtered_dict:
            if isinstance(filtered_dict[key], dict):
                filtered_dict[key] = self.delete_software_elements(filtered_dict[key])

        return filtered_dict

    def sanitize_directory_structure(self, structure: dict, depth: list):
        """Given the directory structure it calculates the SHA256 values for each file."""

        for k in structure:
            new_depth = depth.copy()
            new_depth.append(k)

            if isinstance(structure[k], dict):
                self.sanitize_directory_structure(structure[k], new_depth)
            else:
                filename = reduce(os.path.join, new_depth)
                structure[k] = self.calculate_hash(filename)

    def create_json_files(self, structure: dict, depth: list):
        """Given the directory structure it creates or updates the JSON files with hashes
        and marking files to parse if necessary."""

        for k in structure:
            # we need to have a proper name for the file so we need to store
            # the dict keys, becuse they represent directory and file names
            new_depth = depth.copy()
            new_depth.append(k)

            if isinstance(structure[k], dict):
                # recursively traverse the dict untill entry for a file is found
                self.create_json_files(structure[k], new_depth)
            else:
                filename = "-".join(new_depth)
                filename = ".".join([filename, "json"])
                path = os.path.join(self.JSON_FILES_DIR, filename)

                if not os.path.exists(path):
                    append_write = "w"
                else:
                    append_write = "r+"

                with open(path, append_write) as json_file:
                    json_dict = {}
                    json_dict["hash"] = structure[k]
                    if not append_write == "w":
                        saved_data = json.load(json_file)
                        if saved_data["hash"] == json_dict["hash"]:
                            # hashes are same and it is a known file so we do nothing
                            pass
                        else:
                            # known file, but the hash is different so it needs to be scanned
                            json_dict["scan_file"] = True
                            json_file.seek(0)
                            json_file.truncate()
                            json.dump(json_dict, json_file)
                    else:
                        # It is a new file so we need to scan it
                        json_dict["scan_file"] = True
                        json.dump(json_dict, json_file)

    def parse_files(self):
        """Parses JSON files and if text-file parse is needed performs it."""

        json_files = os.listdir(self.JSON_FILES_DIR)

        for json_file in tqdm(json_files, desc="Parsing files", ncols=121):
            path = os.path.join(self.JSON_FILES_DIR, json_file)
            with open(path, "r+") as opened_file:
                saved_json = json.load(opened_file)
                new_json = {}
                new_json["hash"] = saved_json["hash"]
                if saved_json["scan_file"]:
                    # text-file parsing is needed
                    variables, dependencies = self.parse_single_file(json_file)

                    # updating JSON in a file
                    new_json["scan_file"] = False
                    new_json["variables"] = variables
                    new_json["depends_on"] = dependencies
                    opened_file.seek(0)
                    opened_file.truncate()
                    json.dump(new_json, opened_file)

    def parse_single_file(self, dep_name: str) -> list:
        """It looks for variable assignment in a text file and returns a dict with those 
        variables."""

        tempROOT_DIR = os.path.abspath(os.path.join(self.ROOT_DIR, ".."))
        name_parts = dep_name.split("-")
        file_part = name_parts[-1].split(".")
        name_parts[-1] = (
            ".".join([file_part[0], file_part[1]]) if len(file_part) > 2 else file_part[0]
        )
        file_name = os.path.join(tempROOT_DIR, reduce(os.path.join, name_parts))
        variables_dict = {}
        num_lines = self.count_lines(file_name)
        assign_pattern = "{%(.*?)%}"
        insert_pattern = "%%(.*?)%%"
        dependencies = []
        with open(file_name, "r") as to_parse:
            for line in tqdm(to_parse, total=num_lines, desc="Reading file", ncols=121):
                search_result = re.findall(assign_pattern, line)
                if search_result != []:
                    for assignment in search_result:
                        assignment.strip()
                        var_name, var_value = assignment.split("=")
                        variables_dict[var_name.strip()] = var_value.strip()

                search_result = re.findall(insert_pattern, line)
                if search_result != []:
                    for insert in search_result:
                        dependencies.append(insert.strip())

        return [variables_dict, dependencies]

    def count_lines(self, path):
        """Simple line count for tqdm to look nicely."""

        fp = open(path, "r+")
        buf = mmap.mmap(fp.fileno(), 0)
        lines = 0
        while buf.readline():
            lines += 1

        fp.close()

        return lines
