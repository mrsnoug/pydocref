import os
from tqdm import tqdm
import json
from functools import reduce
import re


class Compiler:
    """
    Class responsible for "compiling" text files.

    Attributes
    ----------
    known_variables : dict 
        dictionary with variables. Keys are full var_names ("depends_on" style).

    Methods
    ----------
    compile():
        method which takes JSON files and associated text files and compiles them into final
        text output.

    access_variables(var_names):
        given a list of needed variables ("depends_on" list from JSON file) it fetches 
        the data from JSON file and then adds them into known_variables dictionary."""

    SOFTWARE_DIR = os.getcwd()
    ROOT_DIR = os.path.abspath(os.path.join(SOFTWARE_DIR, ".."))
    JSON_FILES_DIR = os.path.join(ROOT_DIR, ".dependency")

    OUTPUT_FILES_DIR = os.path.join(ROOT_DIR, ".out")

    def __init__(self):
        if not os.path.exists(self.OUTPUT_FILES_DIR):
            os.mkdir(self.OUTPUT_FILES_DIR)

        self.known_variables = {}

    def compile(self):
        """Handles compiling. Loops over all JSON files and if it required any dependencies
        it fetches them."""

        json_files = os.listdir(self.JSON_FILES_DIR)

        completed = []

        while len(completed) < len(json_files):
            for json_file in tqdm(json_files, desc="Compiling files", ncols=121):
                if json_file not in completed:
                    path = os.path.join(self.JSON_FILES_DIR, json_file)
                    with open(path, "r+") as f:
                        loaded_json = json.load(f)
                        if loaded_json["depends_on"]:
                            self.access_variables(loaded_json["depends_on"])

                        path_parts = json_file.split("-")
                        file_part = path_parts[-1].split(".")
                        path_parts[-1] = (
                            ".".join([file_part[0], file_part[1]])
                            if len(file_part) > 2
                            else file_part[0]
                        )
                        tempROOT_DIR = os.path.abspath(os.path.join(self.ROOT_DIR, ".."))
                        source_path = os.path.join(
                            tempROOT_DIR, reduce(os.path.join, path_parts)
                        )
                        compiled_path = os.path.join(self.OUTPUT_FILES_DIR, path_parts[-1])
                        insert_pattern = "%%(.*?)%%"
                        assign_pattern = "{%(.*?)%}"
                        with open(source_path, "r") as source, open(
                            compiled_path, "w"
                        ) as compiled:
                            for line in source:
                                new_line = line
                                while (
                                    search_res := re.search(insert_pattern, new_line)
                                ) is not None:
                                    match = search_res.group(1).strip()
                                    new_line = new_line.replace(
                                        search_res.group(0), self.known_variables[match]
                                    )
                                while (
                                    search_res := re.search(assign_pattern, new_line)
                                ) is not None:
                                    new_line = new_line.replace(search_res.group(0), "")
                                compiled.write(new_line)
                    completed.append(json_file)

    def access_variables(self, var_names: str):
        """Given the names of the variables it fetches them from the JSON files and adds them
        to known_variables."""

        dir_to_come_back = os.getcwd()
        tempROOT_DIR = os.path.abspath(os.path.join(self.ROOT_DIR, ".."))

        for var_name in tqdm(var_names, desc="Collecting variables", ncols=121):
            # looping through the list of needed variables
            parts = var_name.split(".")
            to_reference = parts.copy()
            variable = parts[-1]
            filename = parts[-2]
            path = reduce(
                os.path.join,
                [part for part in parts if part != parts[-1] and part != parts[-2]],
            )
            path = os.path.join(tempROOT_DIR, path)

            os.chdir(path)
            files = os.listdir()
            for file in files:
                # checking if our filename has an extension and if so changing it to the name
                # with extension
                if file.startswith(filename):
                    filename = file

            os.chdir(dir_to_come_back)

            parts[-2] = filename
            json_filename = "-".join([part for part in parts if part != parts[-1]])
            json_filename = ".".join([json_filename, "json"])

            with open(os.path.join(self.JSON_FILES_DIR, json_filename), "r") as file:
                loaded = json.load(file)

            loaded_variables = loaded["variables"]
            self.known_variables[".".join(to_reference)] = loaded_variables[variable]
